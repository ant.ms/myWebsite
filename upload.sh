#!/bin/bash
# Basic Settings
localPath=.
tmpPath=/tmp/Hugo
createAndDeleteTmpPath=true

# Authentification
serverUsername=$USER
serverIP=192.168.1.16
serverPath=/var/www/ant.lgbt/confused

usePassword=false
serverPassword=

if [ "$1" = "help" ]
then
	echo Usage: hugoUpload [help]
	echo "Warning! This will errase your files in the target"
	echo "         directory, check your configs if they're correct"
else
	## Generate Site
	cd  "$localPath"

	if [ "$createAndDeleteTmpPath" = true ]; then
		echo deleting previous public...
		rm -R public
	fi

	echo generation site...
	hugo

	if [ -f "public" ]; then
		echo creating temporary path...
	mkdir -p $tmpPath
	fi

	echo connecting to server...
	if [ "$usePassword" = true ]; then
		sshfs -o password_stdin $serverUsername@$serverIP:$serverPath $tmpPath <<< $serverPassword
	else
		sshfs $serverUsername@$serverIP:$serverPath $tmpPath
	fi

	### MAKE LOCAL COPY

	echo deleting previous site files...
	rm -r $tmpPath/*

	echo copying to server...
	cp -R public/* $tmpPath

	echo disconnecting...
	umount $tmpPath

	if [ "$createAndDeleteTmpPath" = true ]; then
		echo deleting tmp folder...
		rm -r $tmpPath
	fi
	

	echo done
fi
