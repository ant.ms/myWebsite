---
title: "Game: Text-Based Tic Tac Toe, written in C"
date: 2019-11-27T17:54:12+01:00
draft: false
toc: false
categories:
- Developement
- Games
tags:
- C
- Software
- Developement
---

This is a fully functional TicTacToe game that I wrote in C to work in the console, it has no dependencies and is made very simple. Don't expect an amazing gaming experience as this is mainly just a project for myself and not very focused on the user envoriment at all, it's functional but not very UI-focused.

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/tictactoe).
