---
title: "Game: Flappy Cheff"
date: 2018-02-06T19:39:11+01:00
draft: false
categories:
- Developement
- Games
tags:
- flappy
- 2D
- android
- Developement
---


Flappy Cheff is a simple game where you need to jump between walls without hitting them. How many points do you manage to achieve?

![Flappy Cheff Home Screen](https://stored.ant.lgbt/img/FlappyCheff.png)

Download APK: [FlappyCheff.apk](https://stored.ant.lgbt/files/FlappyCheff.apk)
