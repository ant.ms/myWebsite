---
title: "Program: Bash Script to generate and upload a Hugo site"
date: 2019-12-15T18:07:17+01:00
draft: false
toc: false
categories:
- Developement
tags:
  - script
  - bash
  - hugo
  - website
  - Developement
---

This is a simple script I wrote, mainly for personal use, that automatically uploads generates and uploads a website made in Hugo to a remote server via sshfs.

<!--more-->

Available Configurations:
```
# Basic Settings
localPath=.
tmpPath=/tmp/uploadHugo

# Authentification
serverUsername=$USER
serverIP=YOUR SERVER IP
serverPath=/var/www/html

usePassword=false
serverPassword=YOUR PASSWORD
```

Dependencies:
- hugo
- sshfs

Download [on Gitlab](https://gitlab.com/ConfusedAnt/hugouploader)
