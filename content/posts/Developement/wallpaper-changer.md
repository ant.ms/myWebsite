---
title: "Program: timebased wallpaper changing script based on Python for KDE"
date: 2019-03-28T18:00:00+01:00
draft: false
toc: false
categories:
- Developement
tags:
- Archive
- Software
- Python
- Wallpaper
- Developement
---

This is a small python3 script I wrote in order to set the wallpaper of KDE based of time. With it you can have libe wallpapers live the Mojava-Live wallpaper from Apple. In order to set it up you just have to replace "PATH" and "PICTURES" with their path on your system.

It uses [ksetwallpaper](https://github.com/pashazz/ksetwallpaper) made by [pashazz](https://github.com/pashazz) wich is bundled with it.

Download [here](https://stored.ant.lgbt/files/wallpaper_changer.tar.gz).
