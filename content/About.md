---
title: "About"
date: 2019-12-04T00:00:00+01:00
draft: false
toc: false
---

Welcome to [ant.lgbt](https://ant.lgbt/) this is my personal Website where I post random ~~mostly stupid~~ stuff while pretending to be a professional... Anyways, thank you for visiting and enjoy your stay on this little island in the depths of the global Internet :D

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

## About me

Hey there, my name is Yanik Ammann, also known as 1Yanik3 or ConfusedAnt (yes, I'm very confused basically all of the time). I'm a gay nerd and got some experience with coding stuff like websites or applications (also some simple games). I'm doing an apprenticeship at Siemens as an Application Developer (I really love it so far, it's a lot of fun). I'm interested in Linux, programming and science ~~and boys~~. I spend way too much time on Reddit and talking to all kind of people on Discord (mainly gays, but I'm also open to talk to other people, so feel free to hit me up if you want to talk).

**You can find me at:**
<br>[<i class="fab fa-gitlab"></i> Yanik Ammann](https://gitlab.com/ConfusedAnt)
<br>[<i class="fas fa-envelope"></i> info@1yanik3.com](mailto:info@1yanik3.com)
<br>[<i class="fab fa-reddit"></i> u/1Yanik3](https://www.reddit.com/user/1Yanik3)
<br>[<i class="fab fa-discord"></i> ConfusedAnt#0704](https://discordapp.com/)

I created and hosted a lot of websites since I started programming, you can look at a short list of them [here](/hosting/). All my other projects have posts in the [Blog section](/posts/) of this website. Some of my Project haven't been published on my website yet/ever, if you want to check those out, visit [my GitLab](https://gitlab.com/ConfusedAnt). Have fun exploring my website or wherever it leads you, I'd love to hear your feedback if you have any!

Most of my work is in English but some things are written in German, they are marked by the DE-prefix. Other categories have other prefixes and, of course, every post is tagged and allows you to quickly find posts of similar topics. I hope you enjoy your visit on my small site. No matter how long you plan on staying here, have a nice day and enjoy your live.

### Operating System

In order to get this out of the way: I use Linux as my main operating system. Still not completely sure what desktop I should settle with or what distro fits me best, but at the moment, I'm using [Manjaro](https://manjaro.org/) with the [Plasma desktop (KDE)](https://kde.org/) mainly because of it's really good way of handling themes (I use the [Nord theme](https://www.nordtheme.com/), which by the way is great, you should check it out). Though I don't like the fact that [qt](https://www.qt.io/) is at least somewhat propietary. I used to use Gnome for everything, because of that, most of my recent applications are written for GTK3 and use headerbars (I really like the Gnome design language and even-though I can agree that there are better things at times, in my opinion it's a very good compromise between functionality, performance and looks).

### Hardware

I use multiple PC's, of course. But my main "Workstation" uses a *Intel Core i5-8600K @ 6x 4.7GHz* and a *RTX 2080*. For storage, I have a *512 GB M.2 SSD* and two *4TB HDDs* for my data storage needs. I want to upgrade my CPU though because it's really lagging behind and I hate the fact that I decided to use watercooling for it because it's just unnecesairy and a lot louder than it would otherwise be.

My Server (which you're currently accessing this website on btw) has a *1TB M.2 SSD* from Samsung, which was donated/given to me by a friend of mine and four *4TB HDDs* which are combined using LVM to form a 16TB disk which is my main storage space. As for it's other internals, this `screenfetch` says it all:
```
         _,met$$$$$gg.           host13@AxxivServer
      ,g$$$$$$$$$$$$$$$P.        OS: Debian 10 buster
    ,g$$P""       """Y$$.".      Kernel: x86_64 Linux 4.19.0-6-amd64
   ,$$P'              `$$$.      Uptime: 51d 23h 32m
  ',$$P       ,ggs.     `$$b:    Packages: 1315
  `d$$'     ,$P"'   .    $$$     Shell: bash 5.0.3
   $$P      d$'     ,    $$P     CPU: Intel Core i7-4790 @ 8x 4GHz [27.8°C]
   $$:      $$.   -    ,d$$'     GPU: intel
   $$\;      Y$b._   _,d$P'      RAM: 7464MiB / 23792MiB
   Y$$.    `.`"Y$$$$P"'
   `$$b      "-.__
    `Y$$
     `Y$$.
       `$$b.
         `Y$$b.
            `"Y$b._
                `""""
```

### Licenses

All of my projects are free to use and modify, so feel to fork them or something. If you want to republish them, add a link to my website and/or my GitLab or email, and, if possible, inform me, I'd love to find out what you plan on doing with it. If you have any questions or feedback, send me an email to [info@1yanik3.com](mailto:info@1yanik3.com) or contact me on Discord, I'm always up for talking.
